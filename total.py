#totes les paraules
def Diccionari(spam, nospam):
    totes = {}

    for paraula in spam:
        totes[paraula] = 0
    for paraula in nospam:
        totes[paraula] = 0

    for paraula in spam:
        totes[paraula] += spam[paraula]
    for paraula in nospam:
        totes[paraula] += nospam[paraula]

    return totes
