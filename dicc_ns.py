import os
import random

#paraules que surten als missatges no spam
def Diccionari_NoSpam(): 

    totes_nospam = {}

    fich = os.listdir('nonspam-train/')

    ficheros = []
    
    for i in range(len(fich)):
    
        index = random.randrange(0,len(fich)-1)
        ficheros.append(fich[index])
        
    for fichero in ficheros:
        f = open("nonspam-train/"+fichero)
        paraules = []
        while True:
            linea = f.readline()
            if not linea: break
            paraula = ""
            for par in linea:
                if par != ' ': paraula += par
                else:
                    trobat = False
                    for pari in paraules:
                        if paraula == pari: trobat = True
                    if trobat == False:
                        if totes_nospam.has_key(paraula) == False: totes_nospam[paraula] = 1
                        else: totes_nospam[paraula] += 1
                        paraules.append(paraula)
                    paraula = ""
            if paraula != "":
                trobat = False
                for pari in paraules:
                    if paraula == pari: trobat = True
                if trobat == False:
                    if totes_nospam.has_key(paraula) == False: totes_nospam[paraula] = 1
                    else: totes_nospam[paraula] += 1
                    paraules.append(paraula)
                paraula = ""
    final = {}
    for i in range(62):
        a = max(totes_nospam.values())
        for paraula in totes_nospam:
            if a == totes_nospam[paraula]:
                final[paraula] = totes_nospam[paraula]
                totes_nospam.pop(paraula)
                break
    return final
