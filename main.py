from dicc_s import *
from dicc_ns import *
from total import *
import os

def pars(name):
    tags = []
    f = open(name)
    linea = f.readline()
    paraula = ""
    for par in linea:
        if par != ' ':
            paraula += par
        else:
            tags.append(paraula)
            paraula = ""
    if paraula != "": tags.append(paraula)
    return tags

def xarxa_bayes(name):
    tags = pars(name)
    spam = Diccionari_Spam()
    nospam = Diccionari_NoSpam()
    totes = Diccionari(spam,nospam)
    entrada_spam = []
    for paraula in tags:
        if spam.has_key(paraula) == True: entrada_spam.append(paraula)
    prob_n_s = 0.5
    prob_d_s = 1.
    for paraula in entrada_spam:
        prob_n_s *= spam[paraula]/(350. + 62.*3)
        prob_d_s *= totes[paraula]/(700. + 62*3.)
    if prob_d_s != 0 :prob_spam = prob_n_s/prob_d_s
    else: prob_spam = 0
    entrada_legitima = []
    for paraula in tags:
        if nospam.has_key(paraula) == True: entrada_legitima.append(paraula)
    prob_n_l = 0.5
    prob_d_l = 1.
    for paraula in entrada_legitima:
        prob_n_l *= nospam[paraula]/(350. + 62.*3)
        prob_d_l *= (totes[paraula]+3.)/(700.+62*3.)
    if prob_d_l != 0 : prob_nospam = prob_n_l/prob_d_l
    else: prob_nospam = 0
    if prob_spam > prob_nospam:
        #print "SPAM"
        return 0
    else:
        #print "CORREU NORMAL"
        return 1

def main():
    ficheros = os.listdir('spam-test/')
    cont = 0
    for fichero in ficheros:
        if (xarxa_bayes("spam-test/"+fichero) == 1): cont += 1
    total = 0.
    ratio = 0.
    nom = 0.
    den = 0.
    nom += cont
    den += len(ficheros)
    ratio += nom/den
    ratio = 1 - ratio
    ratio *= 100
    print "ratio SPAM ben classificats:"+str(ratio)+"%"
    ficheros = os.listdir('nonspam-test/')
    cont = 0
    for fichero in ficheros:
        if (xarxa_bayes("nonspam-test/"+fichero) == 0): cont += 1
    total = 0.
    ratio = 0.
    nom = 0.
    den = 0.
    nom += cont
    den += len(ficheros)
    ratio += nom/den
    ratio = 1 - ratio
    ratio *= 100
    print "ratio NO SPAM ben classificats:"+str(ratio)+"%"
    
    
    
            
    
