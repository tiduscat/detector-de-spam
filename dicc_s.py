import os
import random

#paraules que surten als missatges spam
def Diccionari_Spam(): 

    totes_spam = {}

    fich = os.listdir('spam-train/')

    ficheros = []
    
    for i in range(len(fich)):
    
        index = random.randrange(0,len(fich))
        ficheros.append(fich[index])
    
    for fichero in ficheros:
        f = open("spam-train/"+fichero)
        paraules = []
        while True:
            linea = f.readline()
            if not linea: break
            paraula = ""
            for par in linea:
                if par != ' ': paraula += par
                else:
                    trobat = False
                    for pari in paraules:
                        if paraula == pari: trobat = True
                    if trobat == False:
                        if totes_spam.has_key(paraula) == False: totes_spam[paraula] = 1
                        else: totes_spam[paraula] += 1
                        paraules.append(paraula)
                    paraula = ""
            if paraula != "":
                trobat = False
                for pari in paraules:
                    if paraula == pari: trobat = True
                if trobat == False:
                    if totes_spam.has_key(paraula) == False: totes_spam[paraula] = 1
                    else: totes_spam[paraula] += 1
                    paraules.append(paraula)
                paraula = ""
    final = {}
    for i in range(62):
        a = max(totes_spam.values())
        for paraula in totes_spam:
            if a == totes_spam[paraula]:
                final[paraula] = totes_spam[paraula]
                totes_spam.pop(paraula)
                break
    return final
